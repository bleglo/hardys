package fr.ble.hardys;

import java.util.Iterator;

/**
 * Cette interface décrit le comportant d'une classe d'import de références
 * 
 * @author bruno.legloahec
 *
 */
public interface IReferencesImport {

    /**
     * Ajoute une référence
     * 
     * @param reference
     * @return vrai si la référence a été ajoutée
     */
    public boolean addReference(IReference ref);

    /**
     * Ajoute une erreur d'import de référence
     * 
     * @param errorRef
     * @return
     */
    public boolean addError(IReferenceError errorRef);

    /**
     * Retourne le nombre de références
     * 
     * @return
     */
    public int getReferenceCount();

    /**
     * Retourne le nombre de références en erreur
     * 
     * @return
     */
    public int getErrorsReferencesCount();

    /**
     * Retourne un iterator sur les references
     */
    public Iterator<IReference> getReferenceIterator();

    /**
     * Retourne un iterator sur les references en erreur
     */
    public Iterator<IReferenceError> getReferenceErrorIterator();

    /**
     * Stocke le nom du fichier d'import
     * 
     * @return
     */
    public String getFileRef();

    /**
     * Stocke le nom du fichier d'import
     * 
     * @return
     */
    public void setFileRef(String fileName);

}
