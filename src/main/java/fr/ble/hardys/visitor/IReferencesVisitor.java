package fr.ble.hardys.visitor;

import fr.ble.hardys.IReference;
import fr.ble.hardys.IReferenceError;
import fr.ble.hardys.IReferencesImport;

/**
 * Visiteur d'import de la Classe ReferenceImport et de ses composantes
 * 
 * @author bruno.legloahec
 *
 * @param <T>
 */
public interface IReferencesVisitor<T> {
    public abstract T visit(IReferencesImport refImport);

    public abstract T visit(IReference ref);

    public abstract T visit(IReferenceError error);

}
