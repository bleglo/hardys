package fr.ble.hardys.visitor;

import java.util.Iterator;

import org.jdom2.Element;

import fr.ble.hardys.IReference;
import fr.ble.hardys.IReferenceError;
import fr.ble.hardys.IReferencesImport;

/**
 * Visiteur concret pour l'export des références en xml
 * 
 * @author bruno.legloahec
 *
 */
public class ReferencesToXML implements IReferencesVisitor<Element> {

    @Override
    // Génération xml pour la classe principale d'import
    public Element visit(IReferencesImport refImport) {
        // On va considerer que la structure ne peut contenir qu'un seul groupe
        Element report = new Element("report");

        Element inputFile = new Element("inputFile");
        inputFile.setText(refImport.getFileRef());
        report.addContent(inputFile);

        Element references = new Element("references");
        Element errors = new Element("errors");

        // Génération du xml pour les références
        this.visit(refImport.getReferenceIterator(), references);
        // Génération du xml pour les erreurs
        this.visit(refImport.getReferenceErrorIterator(), errors);

        report.addContent(references);
        report.addContent(errors);
        return report;
    }

    /**
     * Appel du visiteur selon l'instance récupérée par l'itérateur passé en paramètre
     * 
     * @param it
     * @param element
     */
    private void visit(Iterator<?> it, Element element) {
        while (it.hasNext()) {

            Object ref = it.next();

            if (ref instanceof IReference)
                element.addContent(((IReference) ref).accept(this));
            else
                element.addContent(((IReferenceError) ref).accept(this));
        }

    }

    /**
     * Visite de la clase Reference
     */
    @Override
    public Element visit(IReference ref) {
        Element e = new Element("reference");
        e.setAttribute("color", ref.getColor().name());
        e.setAttribute("price", Double.toString(ref.getPrice()));
        e.setAttribute("size", Integer.toString(ref.getSize()));
        e.setAttribute("numReference", ref.getRefNumber());
        return e;
    }

    /**
     * Visite de la classe ReferenceError
     */
    @Override
    public Element visit(IReferenceError error) {
        // <error line="5" message="Incorrect value for color">
        // 1462100403;A;100.1;9</error>
        Element e = new Element("error");
        e.setAttribute("line", Integer.toString(error.getErrorLineNumber()));
        e.setAttribute("message", error.getErrorMsg());
        e.setText(error.getRawLineError());
        return e;
    }

}
