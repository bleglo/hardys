package fr.ble.hardys.visitor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ble.hardys.IReference;
import fr.ble.hardys.IReferenceError;
import fr.ble.hardys.IReferencesImport;

/**
 * Visiteur concret pour l'export des références en xml
 * 
 * @author bruno.legloahec
 *
 */
public class ReferencesToJson implements IReferencesVisitor<String> {

    @Override
    public String visit(IReferencesImport refImport) {
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println();
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(refImport);
        } catch (JsonProcessingException e) {
            return null;
        }

    }

    @Override
    public String visit(IReference ref) {
        // Non implémenté
        return null;
    }

    @Override
    public String visit(IReferenceError error) {
        // Non implémenté
        return null;
    }

}
