package fr.ble.hardys;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import fr.ble.hardys.visitor.IReferencesVisitor;

/**
 * Modélisation d'une référence
 * 
 * @author bruno.legloahec
 *
 */
@JsonPropertyOrder({ "numReference", "size", "price", "type" })
public interface IReference {

    @JsonGetter("numReference")
    public String getRefNumber();

    public void setRefNumber(String refNumber);

    @JsonGetter("type")
    public ReferenceColor getColor();

    public void setColor(ReferenceColor color);

    public int getSize();

    public void setSize(int size);

    public double getPrice();

    public void setPrice(double price);

    /**
     * Visiteur permettant de parcourir la structure de diférentes façon
     * 
     * @param visitor
     * @return
     */
    public abstract <T> T accept(IReferencesVisitor<T> visitor);

}
