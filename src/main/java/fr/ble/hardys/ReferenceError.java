package fr.ble.hardys;

import fr.ble.hardys.visitor.IReferencesVisitor;

/*
 * Classe représentant les références en erreur trouvées lors du parsing
 */
public class ReferenceError implements IReferenceError {

    private int    errorLineNumber;
    private String rawLineError;
    private String errorMsg;

    public ReferenceError(int lineNumber, String message, String line) {
        this.errorLineNumber = lineNumber;
        this.errorMsg = message;
        this.rawLineError = line;
    }

    @Override
    public int getErrorLineNumber() {
        return this.errorLineNumber;
    }

    @Override
    public void setErrorLineNumber(int lineNumber) {
        this.errorLineNumber = lineNumber;
    }

    @Override
    public String getRawLineError() {
        return this.rawLineError;
    }

    @Override
    public void setRawLineError(String line) {
        this.rawLineError = line;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMsg;
    }

    @Override
    public void setErrorMsg(String msg) {
        this.errorMsg = msg;
    }

    @Override
    public <T> T accept(IReferencesVisitor<T> visitor) {
        // TODO Auto-generated method stub
        return visitor.visit(this);
    }

}
