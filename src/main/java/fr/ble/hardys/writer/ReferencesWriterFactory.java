package fr.ble.hardys.writer;

import java.util.Arrays;

import fr.ble.hardys.IReferencesImport;

/**
 * Fabrique chargée d'instancier un write de R
 * 
 * @author bruno.legloahec
 *
 */
public class ReferencesWriterFactory {

    public static final String   FILE_JSON = "json";
    public static final String   FILE_XML  = "xml";

    public static final String[] formats   = { FILE_JSON, FILE_XML };

    public static IReferencesWriter build(IReferencesImport references, String format) throws ReferenceWriterUnknownFormatException {

        if (format.equals(FILE_JSON))
            return new ReferenceWriterJson(references);

        if (format.equals(FILE_XML))
            return new ReferenceWriterXml(references);

        String msg = "Format inconnu, " + Arrays.toString(ReferencesWriterFactory.formats);
        throw new ReferenceWriterUnknownFormatException(msg);

    }

}
