package fr.ble.hardys.writer;

/**
 * Représente une classe chargée d'écrire un fichier de Reference
 * 
 * @author bruno.legloahec
 *
 */
public interface IReferencesWriter {

    public void write(String fileName);

}
