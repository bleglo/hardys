package fr.ble.hardys.writer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.output.support.AbstractXMLOutputProcessor;
import org.jdom2.output.support.FormatStack;
import org.jdom2.output.support.XMLOutputProcessor;

import fr.ble.hardys.IReferencesImport;
import fr.ble.hardys.visitor.ReferencesToXML;

public class ReferenceWriterXml extends AReferenceWriter implements IReferencesWriter {

    public ReferenceWriterXml(IReferencesImport references) {
        super(references);
    }

    @Override
    public void write(String fileName) {

        final XMLOutputProcessor XMLOUTPUT = new AbstractXMLOutputProcessor() {
            @Override
            protected void printDeclaration(final Writer out, final FormatStack fstack) throws IOException {
                write(out, "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?> ");
                write(out, fstack.getLineSeparator());
            }
        };

        XMLOutputter out = new XMLOutputter(Format.getPrettyFormat(), XMLOUTPUT);
        ReferencesToXML xmlVisitor = new ReferencesToXML();
        Document doc = new Document(xmlVisitor.visit(this.references));
        FileWriter file = null;
        try {
            file = new FileWriter(new File(fileName));
            out.output(doc, file);

        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (null != file)
                try {
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

    }

}
