package fr.ble.hardys.writer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import fr.ble.hardys.IReferencesImport;
import fr.ble.hardys.visitor.ReferencesToJson;

public class ReferenceWriterJson extends AReferenceWriter implements IReferencesWriter {

    public ReferenceWriterJson(IReferencesImport references) {
        super(references);
    }

    @Override
    public void write(String fileName) {
        ReferencesToJson jsonVisitor = new ReferencesToJson();
        FileWriter file = null;
        try {
            file = new FileWriter(new File(fileName));
            file.write(jsonVisitor.visit(this.references));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != file)
                try {
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

    }

}
