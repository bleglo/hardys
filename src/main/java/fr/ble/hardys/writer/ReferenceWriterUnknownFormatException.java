package fr.ble.hardys.writer;

public class ReferenceWriterUnknownFormatException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ReferenceWriterUnknownFormatException(String message) {
        super(message);
    }
}
