package fr.ble.hardys;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "inputFile", "references", "errors" })
public class ReferencesImport implements IReferencesImport {
    private List<IReference>      refList   = new ArrayList<IReference>();
    private List<IReferenceError> errorList = new ArrayList<IReferenceError>();
    private String                fileRef   = new String("");

    @JsonGetter("references")
    public List<IReference> getReflist() {
        return this.refList;
    }

    @JsonGetter("errors")
    public List<IReferenceError> getErrorList() {
        return this.errorList;
    }

    @Override
    public boolean addReference(IReference ref) {
        return refList.add(ref);
    }

    @Override
    public boolean addError(IReferenceError errorRef) {
        return errorList.add(errorRef);
    }

    @JsonIgnore
    @Override
    public int getReferenceCount() {
        return refList.size();
    }

    @JsonIgnore
    @Override
    public int getErrorsReferencesCount() {
        return errorList.size();
    }

    @JsonIgnore
    @Override
    public Iterator<IReferenceError> getReferenceErrorIterator() {
        return errorList.iterator();
    }

    @JsonIgnore
    @Override
    public Iterator<IReference> getReferenceIterator() {
        return refList.iterator();
    }

    @Override
    @JsonGetter("inputFile")
    public String getFileRef() {
        return this.fileRef;
    }

    @Override
    public void setFileRef(String fileName) {
        this.fileRef = fileName;
    }

}
