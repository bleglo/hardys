package fr.ble.hardys;

import fr.ble.hardys.visitor.IReferencesVisitor;

/**
 * Une référence composée d'une référence, d'une couleur, d'un prix et d'une taille
 * 
 * @author bruno.legloahec
 *
 */
public class Reference implements IReference {

    private String         refNumber;
    private ReferenceColor color;
    private int            size;
    private double         price;

    public Reference(String refNumber, ReferenceColor color, int size, Double price) {
        this.refNumber = refNumber;
        this.color = color;
        this.size = size;
        this.price = price;

    }

    @Override
    public String getRefNumber() {
        return refNumber;
    }

    @Override
    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    @Override
    public ReferenceColor getColor() {
        return color;
    }

    @Override
    public void setColor(ReferenceColor color) {
        this.color = color;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public <T> T accept(IReferencesVisitor<T> visitor) {
        // TODO Auto-generated method stub
        return visitor.visit(this);
    }

}
