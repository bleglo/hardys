package fr.ble.hardys;

/**
 * Couleurs supportées dans le fichier d'entrée
 * 
 * @author bruno.legloahec
 *
 */
public enum ReferenceColor {

    R("Rouge"),
    G("Green"),
    B("Bleu"),
    Unknown("X");

    private String chaine;

    private ReferenceColor(String s) {
        this.chaine = s;
    }

    /**
     * 
     * @param s
     *            : le code de la couleur
     * @return l'enum correspondante
     */
    public String getColorText() {
        return this.chaine;
    }
}
