package fr.ble.hardys.parser;

import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import fr.ble.hardys.IReferenceError;
import fr.ble.hardys.IReferencesImport;
import fr.ble.hardys.Reference;
import fr.ble.hardys.ReferenceColor;
import fr.ble.hardys.ReferenceError;
import fr.ble.hardys.ReferencesImport;

/**
 * Parser de références en format point-virgule
 */
public class SemiColonReferenceParser implements IReferencesParser {

    FileReader fileReader;

    public SemiColonReferenceParser(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    @Override
    public IReferencesImport parse() {
        IReferencesImport refs = new ReferencesImport();
        int lineNumber = 1;

        CSVParser parser = new CSVParserBuilder().withSeparator(';').withIgnoreQuotations(true).build();

        CSVReader reader = new CSVReaderBuilder(this.fileReader).withSkipLines(0).withCSVParser(parser).build();

        try {
            for (String[] line : reader.readAll()) {
                try {
                    // La référence contient 4 champs
                    // S'il y en a plus, on ignore le reste
                    if (line.length < 4)
                        throw new ReferenceParsingException("Reference cannot be parsed");
                    String refNumber = line[0];
                    // La référence ne doit pas dépasser 10 caractères
                    if (refNumber.length() > 10)
                        throw new ReferenceRefNumberTooLongException("RefNumber is too long");
                    ReferenceColor color;
                    try {
                        // On essaye de retrouver la couleur à partir de l'enum
                        color = ReferenceColor.valueOf(line[1]);
                    } catch (Exception e) {
                        throw new ReferenceInvalidColorException("Incorrect value for color");
                    }
                    Double price;
                    try {
                        price = Double.parseDouble(line[2]);
                    } catch (NumberFormatException e) {
                        throw new ReferenceInvalidPriceException("Incorrect value for price");
                    }
                    int size;
                    try {
                        size = Integer.parseInt(line[3]);
                    } catch (Exception e) {
                        throw new ReferenceInvalidSizeException("Incorrect value for price");
                    }
                    refs.addReference(new Reference(refNumber, color, size, price));

                } catch (Exception e) {
                    IReferenceError error = new ReferenceError(lineNumber, e.getMessage(), String.join(";", line));
                    refs.addError(error);
                }

                lineNumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return refs;
    }

}
