package fr.ble.hardys.parser;

/**
 * Exception levée lorsque qu'une couleur inconnue est trouvée pendant le parsing
 * 
 * @author bruno.legloahec
 *
 */
public class ReferenceInvalidPriceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ReferenceInvalidPriceException(String message) {
        super(message);
    }

}
