package fr.ble.hardys.parser;

import fr.ble.hardys.IReferencesImport;

/**
 * Interface permettant d'implementer un parser de fichier de références
 * 
 * @author bruno.legloahec
 *
 */
public interface IReferencesParser {
    /**
     * Permet de parser
     * 
     * @return un objet de type IReferenceImport contenant les références et les erreurs récupérées lors du parsing
     */
    public IReferencesImport parse();
}
