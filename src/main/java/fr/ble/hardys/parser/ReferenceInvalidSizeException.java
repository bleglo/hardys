package fr.ble.hardys.parser;

/**
 * Exception levée lorsque qu'une couleur inconnue est trouvée pendant le parsing
 * 
 * @author bruno.legloahec
 *
 */
public class ReferenceInvalidSizeException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ReferenceInvalidSizeException(String message) {
        super(message);
    }

}
