package fr.ble.hardys.parser;

/**
 * Exception levée quand la référence de l'article est trop longue
 * 
 * @author bruno.legloahec
 *
 */
public class ReferenceRefNumberTooLongException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ReferenceRefNumberTooLongException(String message) {
        super(message);
    }

}
