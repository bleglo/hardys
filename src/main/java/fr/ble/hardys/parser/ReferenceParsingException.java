package fr.ble.hardys.parser;

/**
 * Exception levée lorsque qu'il manque des informations pour pouvoir instancier un article
 * 
 * @author bruno.legloahec
 *
 */
public class ReferenceParsingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ReferenceParsingException(String message) {
        super(message);
    }

}
