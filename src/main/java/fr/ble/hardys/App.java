package fr.ble.hardys;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

import fr.ble.hardys.parser.SemiColonReferenceParser;
import fr.ble.hardys.writer.IReferencesWriter;
import fr.ble.hardys.writer.ReferenceWriterUnknownFormatException;
import fr.ble.hardys.writer.ReferencesWriterFactory;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("You must provide the input file's path " + ", the output format (" + Arrays.toString(ReferencesWriterFactory.formats) + ") " + "and the output file's path");
            System.exit(1);
        }
        String entree = args[0];
        String format = args[1];
        String sortie = args[2];

        entree = entree.replace("\\", "/");
        sortie = sortie.replace("\\", "/");
        String[] tokens = entree.split("/");
        String fileName = tokens[tokens.length - 1];

        try {
            IReferencesImport references = new SemiColonReferenceParser(new FileReader(entree)).parse();
            references.setFileRef(fileName);
            IReferencesWriter writer;
            writer = ReferencesWriterFactory.build(references, format);
            writer.write(sortie);
        } catch (ReferenceWriterUnknownFormatException e) {
            System.out.println("Unknown output file's format.");

            System.out.println("Formats list supported : " + Arrays.toString(ReferencesWriterFactory.formats));

        } catch (FileNotFoundException e) {
            System.out.println(String.format("File %s does not exist", fileName));
        }

    }
}
