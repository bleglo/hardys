package fr.ble.hardys;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import fr.ble.hardys.visitor.IReferencesVisitor;

@JsonPropertyOrder({ "line", "message", "value" })
public interface IReferenceError {

    /**
     * 
     * @return le numéro de la ligne du fichier en erreur
     */

    @JsonGetter("line")
    public int getErrorLineNumber();

    /**
     * 
     * @param lineNumber
     *            le numéro de la ligne du fichier en erreur
     * 
     */
    public void setErrorLineNumber(int lineNumber);

    /**
     * 
     * @return la ligne brute en erreur du fichier
     */
    @JsonGetter("value")
    public String getRawLineError();

    /**
     * la ligne brute en erreur du fichier
     * 
     * @param line
     */
    public void setRawLineError(String line);

    /**
     * 
     * @return Le message d'erreur de la ligne qui a provoqué l'erreur
     */
    @JsonGetter("message")
    public String getErrorMsg();

    /**
     * Le message d'erreur de la ligne qui a provoqué l'erreur
     * 
     * @param msg
     */

    public void setErrorMsg(String msg);

    public abstract <T> T accept(IReferencesVisitor<T> visitor);

}
