package fr.ble.hardys;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import fr.ble.hardys.parser.SemiColonReferenceParser;
import junit.framework.TestCase;

public class TestImportFichier extends TestCase {

    private static final String filename = "src/test/resources/reference.txt";
    private IReferencesImport   references;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        try {
            references = new SemiColonReferenceParser(new FileReader(filename)).parse();
            references.setFileRef(filename);
        } catch (FileNotFoundException e) {
            System.out.println("Fichier introuvable");
        }
    }

    public void testImport() {

        assertEquals("Nous devons trouver 4 refs ", 4, references.getReferenceCount());
        assertEquals("Nous devons trouver 1 ref en erreur ", 1, references.getErrorsReferencesCount());

        // Test la référence en erreur
        IReferenceError error = references.getReferenceErrorIterator().next();
        assertEquals("Ligne brute", "1462100403;A;100.1;9", error.getRawLineError());
        assertEquals("Message d'erreur", "Incorrect value for color", error.getErrorMsg());
        assertEquals("Numéro de la ligne en erreur", 5, error.getErrorLineNumber());

        // On va tester la 3ème référence
        // 1462100044;G;5.56;19
        Iterator<IReference> it = references.getReferenceIterator();
        it.next();
        it.next();
        IReference ref = it.next();
        assertEquals("RefNumber", "1462100044", ref.getRefNumber());
        assertEquals("Couleur", ReferenceColor.G, ref.getColor());
        assertEquals("Taille", 19, ref.getSize());
        assertEquals("Prix", 5.56, ref.getPrice());

    }

}
