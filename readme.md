## Vous trouverez dans ce répertoire : 

+ le source du projet
+ un exemple de fichier à importer à la racine : reference.txt
+ le fichier jar généré dans target : reference.jar
+ 2 fichiers de résultats


## Invocation du jar
java -jar target/reference.jar reference.txt json reference.json

## Compilation
mvn install assembly:single


Le project est compatible à partir de la version 1.6 de java
